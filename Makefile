build:
	go build -trimpath -o bin/downhub main.go

dev:
	go run main.go

build-image:
	docker build -t resium-downhub:latest .