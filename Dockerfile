FROM golang:1.15-alpine
LABEL maintainer="hsowan <hsowan2019@gmail.com>"
EXPOSE 8080
WORKDIR /data/resium-downhub
COPY . .
RUN apk add --no-cache git && \
    go mod download && \
    go build -o downhub main.go && \
    apk del git
CMD ["./downhub"]
