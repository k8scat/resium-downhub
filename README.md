# DownHub

Fork from https://github.com/gufeijun/baiduwenku-go

## Start

```shell script
# Dev
go run main.go

# Deploy
docker-compose up -d

```

## Support

- [x] 百度文库

## Techs

- https://github.com/gin-gonic/gin


