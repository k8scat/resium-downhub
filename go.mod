module github.com/k8scat/downhub

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/tidwall/gjson v1.6.1
)
